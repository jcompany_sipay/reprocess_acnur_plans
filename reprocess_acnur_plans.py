import argparse
import pymongo
import logging
from bson.objectid import ObjectId
from datetime import datetime, timedelta
from urllib import parse as urllib_parse


def escape_password(uri):
    if uri is None:
        return None

    if uri == 'localhost' or '@' not in uri:
        return uri

    if not uri.startswith('mongodb://'):
        uri = 'mongodb://%s' % uri

    password = '@'.join(uri.split('@')[0:-1]).split(':', 2)[2]

    return uri.replace(password, urllib_parse.quote_plus(password))


def reprocess_bcps(event_id, collection):
    """Update BCPS database in order to force reprocess of a plan"""
    # Update the plan so it is executed again
    res = collection.update_one(
        filter={"event_id": event_id},
        update={"$set": {
            'completed_at': None,
            'operations': []
        }}
    )
    # Check if the update was succesful
    if res.modified_count == 1:
        logger.info(
            "Se ha actualizado el evento con ID: '%s' en BCPS" % event_id)
        done.write(event_id + "\n")
        return True
    else:
        logger.warning(
            "Fallo al actualizar el documento con event_id: '%s' en BCPS "
            "(O no existe el evento con ese ID, o ya estaba modificado)"
            % event_id)
        return False


def reprocess_sepa(event_id, collection):
    """Update SEPA database in order to force reprocess of a plan"""
    # Update the plan so it is executed again
    res = collection.update_one(
        filter={"reference": event_id},
        update={"$set": {
            "status": "pending",
            "send_date": datetime.now().replace(
                hour=0, minute=0, second=0, microsecond=0) + timedelta(days=1)
        }}
    )

    # Check if the update was succesful
    if res.modified_count == 1:
        logger.info(
            "Se ha actualizado el evento con ID: '%s' en SEPA" % event_id)
        done.write(event_id + "\n")
        return True
    else:
        logger.warning(
            "Fallo al actualizar el documento con event_id: '%s' en SEPA "
            "(O no existe el evento con ese ID, o ya estaba modificado)"
            % event_id)
        return False


def reprocess_plan(plan, plan_collection, sepa_collection, bcps_collection):
    """Check plan type and reprocess it"""
    if plan["events"][0]["completed_at"]:
        pos = 0
    elif plan["events"][1]["completed_at"]:
        pos = 1
    else:
        logger.error("No se ha encontrado la operación a actualizar del plan "
                     "con ID: '%s'" % plan["_id"])
        return False

    # If the operation to be reprocessed was succesful -> skip
    if (len(plan["events"][pos].get("operations", [])) > 0 and
            plan["events"][pos]["operations"][0].get("result_code") == 0):
        logger.error("El plan con ID: '%s' ya ha sido previamente procesado "
                     "correctamente" % plan["_id"])
        return False

    event_id = plan["events"][pos]["_id"]
    if plan["method"]["type"] == "debit":
        reprocessed = reprocess_sepa(event_id, sepa_collection)
    elif plan["method"]["type"] == "card":
        reprocessed = reprocess_bcps(event_id, bcps_collection)
    else:
        logger.error("El plan con ID: '%s' tiene tipo de metodo "
                     "incorrecto" % plan["_id"])
        return False

    if reprocessed:
        updated = plan_collection.update_one(
            filter={"_id": plan["_id"]},
            update={"$set": {"events.%s.completed_at" % pos: None}}
        )
        # Check if the update was succesful
        if updated.modified_count == 1:
            logger.info("Se ha actualizado el plan con ID: '%s'" % plan["_id"])
            return True
        else:
            logger.warning(
                "Fallo al actualizar el plan con ID: '%s'" % plan["_id"])
            return False
    return False


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='Changes recu plans documents')
    parser.add_argument('-d', '--debug', action='store_true')
    parser.add_argument('-u1', '--uri_recu', default='localhost')
    parser.add_argument('-u2', '--uri_sepa')
    parser.add_argument('-u3', '--uri_bcps')
    parser.add_argument('-p', '--plan_list',
                        help='Optional filepath with a doc ID per line')
    options = parser.parse_args()

    logger = logging.getLogger(__file__)
    formatter = logging.Formatter(
        fmt='{asctime} {levelname:>8} - {name}({lineno}): {message}',
        style='{')

    console = logging.StreamHandler()
    console.setFormatter(formatter)

    file_handler = logging.FileHandler('reprocess_log.log')
    file_handler.setFormatter(formatter)

    logger.addHandler(console)
    logger.addHandler(file_handler)

    logger.setLevel(logging.DEBUG if options.debug else logging.INFO)
    logger.info('%s launched.' % __file__)

    uri_recu = escape_password(options.uri_recu)
    options.uri_sepa = escape_password(options.uri_sepa) or uri_recu
    options.uri_bcps = escape_password(options.uri_bcps) or uri_recu

    logger.debug('Conectando a Mongo ...')
    plan_collection = pymongo.MongoClient(uri_recu)["recu"]["plans"]
    sepa_collection = pymongo.MongoClient(options.uri_sepa)["sepa"]["debits"]
    bcps_collection = pymongo.MongoClient(options.uri_bcps)["bcps"]["events"]

    logger.debug('Conectado.')

    done = open("done_%s.txt" % datetime.now().strftime('%Y%m%d%H%M%S'), 'a')

    plan_list = options.plan_list

    # Read the target plan IDs
    with open(plan_list) as f:
        plans = [e.replace('\n', '').replace('\r', '') for e in f.readlines()]
    id_plans = [ObjectId(p) for p in plans]

    # Get the plan objects from mongo for each of the plan IDs
    plans_cursor = plan_collection.find({
        "_id": {
            "$in": id_plans
        }
    })
    i, j = 0, 0
    # Reprocess each of the plans
    for plan in plans_cursor:
        j += 1
        if reprocess_plan(plan, plan_collection,
                          sepa_collection, bcps_collection):
            i += 1

    logger.info("Procesados correctamente %s/%s casos" % (i, j))
