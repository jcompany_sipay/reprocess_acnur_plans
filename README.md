# Acnur plan reprocesser

Script used to change failed Acnur plans in order to reprocess them


# Usage

```bash
pipenv install
pipenv run python reprocess_acnur_plans.py [-d] [-u mongodb://user:pass@host:port] -p plan_list
```

The `plan_list` file expects a list of plan IDs separated by break-lines

    planID_1
    planID_2
    planID_3
    ...

To enable debug mode set add the `-d` flag

# Distributing

Distribution may be done in the usual setuptools way.
If you don't want to use pipenv, just use requirements.txt file as usual and
remove Pipfile, setup.py will auto-detect Pipfile removal and won't try to
update requirements.

Note that, to enforce compatibility between PBR and Pipenv, this updates the
tools/pip-requires and tools/test-requires files each time you do a *dist*
command

# General notes

This package uses Pipenv. Pipenv can be easily replaced by a virtualenv by keeping requirements.txt
instead of using pipenv flow.
